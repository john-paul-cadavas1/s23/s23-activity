db.rooms.insertOne({
	"name": "Single",
	"accomodates": 2,
	"description": "A simple room with all the basic necessities",
	"roomsAvailable": 10,
	"isAvailable": false
});



db.rooms.insertMany([
		{
			"name": "Double",
			"accomodates": 3,
			"description": "A room fit for a small family going on a vacation",
			"roomsAvailable": 5,
			"isAvailable": false
		},
		{
			"name": "Queen",
			"accomodates": 4,
			"description": "A room with a queen sized bed perfect for a simple getaway",
			"roomsAvailable": 15,
			"isAvailable": false
		}
	]);


db.rooms.findOne({
	"name": "Double",
});



db.rooms.updateOne(
		{
			"name": "Queen"
		},
		{
			$set: {
				"roomsAvailable": 0,
			}
		}
	)



db.rooms.deleteMany({
	"roomsAvailable": 0
});


